﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DropTrap : MonoBehaviour {
    private Animator controller;

    public AudioClip smash;
    public AudioClip death;

    public float DamageMultiplier=1f;

    void Activate()
    {
        controller.SetTrigger("activate");
    }

    public void Smash()
    {
        AudioSource.PlayClipAtPoint(smash, transform.position);
    }

    void Impact(Collision collision)
    {
        Health health = collision.gameObject.GetComponent<Health>();
        if (health!=null && collision.gameObject.tag!="Trap Structure")
        {
            health.TakeDamage(collision.relativeVelocity.magnitude*DamageMultiplier);
        }
    }

	// Use this for initialization
	void Awake () {
        controller = GetComponent<Animator>();
	}
	
	void Death() {
        AudioSource.PlayClipAtPoint(death, transform.position);
        Destroy(gameObject);
	}
}
