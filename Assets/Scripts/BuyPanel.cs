﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyPanel : MonoBehaviour {

    public Transform Content;

    public Transform playerParent;

    public BuyObject selectedObject = null;
    private BuyObjectDisplay currentDisplay = null;

    public BuyObjectDisplay prefab;

    private BuyObject preview = null;

    void Start()
    {
        foreach (BuyObject obj in Resources.LoadAll<BuyObject>(""))
        {
            BuyObjectDisplay disp = Instantiate<BuyObjectDisplay>(prefab);
            disp.transform.SetParent(Content, false);
            disp.Populate(obj);
            Button button=disp.gameObject.AddComponent<Button>();
            button.onClick.AddListener(() => SetSelectedObject(disp,obj));
        }
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    void SetSelectedObject (BuyObjectDisplay disp, BuyObject obj)
    {
        if (currentDisplay!=null)
        {
            currentDisplay.GetComponent<Image>().color = Color.white;
        }
        selectedObject = obj;
        if (disp != null)
        {
            disp.GetComponent<Image>().color = Color.red;
            currentDisplay = disp;
        }
        if (preview)
        {
            Destroy(preview.gameObject);
        }
        if (obj != null)
        {
            preview = Instantiate(obj);
            preview.SetObjectPreview();
            preview.name = "preview";
        }
    }

    private bool mouseDown=false;
    private GameManager gm;

    private void Update()
    {
        if (selectedObject!=null)
        {
            Ray ray=Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rHit;
            Vector3 pos=Vector3.zero;
            bool valid = false;
            if (Physics.Raycast(ray, out rHit, 300, 256))
            {
                pos = rHit.point;
                preview.transform.position = pos;
                preview.gameObject.SetActive(true);
                valid = true;
            } else
            {
                if (preview!=null)
                {
                    preview.gameObject.SetActive(false);
                }
                valid = false;
            }
            if (Input.GetMouseButton(0))
            {
                if (mouseDown==false && valid && preview.IsValidPlacement && gm.Buy(selectedObject.cost))
                {
                    //TODO Check to see if we have funds!
                    BuyObject newObject= Instantiate(selectedObject);
                    newObject.transform.position = pos;
                    newObject.transform.parent = playerParent;
                    newObject.transform.rotation = preview.transform.rotation;
                }
                mouseDown = true;
            } else
            {
                mouseDown = false;
            }
            if (Input.GetMouseButton(1) || Input.GetKey(KeyCode.Escape))
            {
                SetSelectedObject( null,null);
            }
        }
        
    }

    public void Rotate(int degrees)
    {
        preview.transform.Rotate(new Vector3(0,degrees, 0));
    }
}
