﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Navigation : MonoBehaviour {

    public GameObject target;
    private NavMeshAgent navMesh;

    // Use this for initialization
    void Start () {
        navMesh=GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            navMesh.destination = target.transform.position;
        }
	}
}
