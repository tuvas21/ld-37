﻿using UnityEngine;
using UnityEngine.AI;

public class BuyObject : MonoBehaviour {
    public int cost;
    public Texture2D texture;
    public Renderer[] renderers;
    public Collider[] colliders;

    bool isInPreviewMode = false;

    public IsCurrentlyTriggered BlockZone;

    private Color[] colors;

    public Color AdjustColorForValid= Color.Lerp(Color.gray, Color.clear, 0.5f);
    public Color AdjustColorForInvalid= Color.Lerp(Color.red, Color.clear, 0.5f);

    public void SetObjectPreview()
    {
        isInPreviewMode = true;
        colors = new Color[renderers.Length];
        for (int i=0;i<renderers.Length;i++){
            colors[i] = renderers[i].material.color;
            renderers[i].material.color*=Color.Lerp(Color.gray,Color.clear,0.5f);
            renderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
        foreach (Collider collider in colliders)
        {
            collider.enabled=false;
        }
        foreach (NavMeshObstacle obst in gameObject.GetComponentsInChildren<NavMeshObstacle>())
        {
            obst.enabled = false;
        }

        gameObject.AddComponent<Rigidbody>().isKinematic = true;
    }

    private void Update()
    {
        if (isInPreviewMode)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.color = colors[i]*(BlockZone.IsTriggered?AdjustColorForInvalid:AdjustColorForValid);
            }
        }
    }

    public bool IsValidPlacement
    {
        get
        {
            return BlockZone.IsTriggered == false;
        }
    }
}
