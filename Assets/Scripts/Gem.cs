﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Gem : MonoBehaviour {

    public AudioClip death;

    private void OnDestroy()
    {
        AudioSource.PlayClipAtPoint(death, transform.position);
    }
}
