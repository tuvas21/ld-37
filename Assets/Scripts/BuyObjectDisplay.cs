﻿using UnityEngine;
using UnityEngine.UI;

public class BuyObjectDisplay : MonoBehaviour {

    public Text text;
    public RawImage image;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Populate(BuyObject obj)
    {
        text.text = "$" + obj.cost;
        image.texture = obj.texture;
    }
}
