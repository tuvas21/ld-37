﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {
    public float keepTime;

    private float deathTime;

	// Use this for initialization
	void Start () {
        deathTime = Time.time+keepTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > deathTime) {
            Destroy(gameObject);
        }
	}
}
