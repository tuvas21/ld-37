﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsCurrentlyTriggered : MonoBehaviour {

    public bool IsTriggered { get; protected set; }

    public void StartTest()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        IsTriggered = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        IsTriggered = true;
    }

    private void OnTriggerExit(Collider other)
    {
        IsTriggered = false;
    }
}
