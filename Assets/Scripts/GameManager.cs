﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    //public int cash { get; protected set; }
    public int cash;

    private int killCount = 0;

    public Text CashDisplay;
    public Text KillDisplay;

    // Use this for initialization
    void Start () {
        cash = 10;
        Enemy.allDeathListener += Death;
	}
	
	// Update is called once per frame
	void Update () {
	    if (GameObject.FindGameObjectsWithTag("Goal").Length==0)
        {
            SceneManager.LoadScene("Game Over");
        }
        CashDisplay.text = "$"+cash;
        KillDisplay.text = killCount.ToString();
	}

    void Death(int reward)
    {
        killCount += 1;
        cash += reward;
    }

    public bool Buy(int amount)
    {
        if (amount<=cash)
        {
            cash -= amount;
            return true;
        }
        return false;
    }
}
