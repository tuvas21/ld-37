﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Navigation))]
public class Enemy : MonoBehaviour {
    public int rewardForDeath;

    public float damage = 1f;

    public delegate void DeathListener(int reward);

    public static event DeathListener allDeathListener;

    public AudioClip deathClip;

    void OnCollisionEnter(Collision collision)
    {
        ProcessCollision(collision.gameObject);
    }

    void ProcessCollision(GameObject obj)
    {
        if (obj.tag=="Enemy" || obj.tag=="Floor")
        {
            return;
        }
        if (obj.tag == "Goal")
        {
            Destroy(obj.gameObject);
        }
        Health health = obj.GetComponent<Health>();
        if (health != null)
        {
            health.TakeDamage(damage);
        }
    }

    private void OnEnable()
    {
        SetGoal();
    }

    private void Update()
    {
        if (GetComponent<Navigation>().target==null) {
            SetGoal();
        }
    }

    void Dead()
    {
        if (allDeathListener!=null)
        {
            allDeathListener(rewardForDeath);
        }
        SendMessage("DeathEffect");
        AudioSource.PlayClipAtPoint(deathClip, transform.position);
    }

    private void SetGoal()
    {
        GameObject[] goals = GameObject.FindGameObjectsWithTag("Goal");
        GameObject[] trapPoints = GameObject.FindGameObjectsWithTag("TrapStructure");
        if (Random.value>0.7f && trapPoints.Length>0 && damage>0)
        {
            goals = trapPoints;
        }
        if (goals.Length > 0)
        {
            GetComponent<Navigation>().target = goals[Random.Range(0, goals.Length)];
        }
    }
}
