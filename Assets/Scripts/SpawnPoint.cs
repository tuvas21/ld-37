﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public float spawnRate=5f;

    public Enemy[] enemyPrefabs;

    public Transform RobotParent;

    public float nextSpawnTime = 0f;

    private void Start()
    {
        nextSpawnTime = Time.time;
        SetNextTime();
    }

    // Update is called once per frame
    void SetNextTime()
    {
        nextSpawnTime += Random.Range(spawnRate / 2, spawnRate * 3 / 2);
        spawnRate *= .95f;
        spawnRate = Mathf.Max(0.4f, spawnRate);
    }
	
	// Update is called once per frame
	void Update () {
		while (nextSpawnTime<Time.time)
        {
            int rID = Random.Range(0, enemyPrefabs.Length);
            Enemy obj = Instantiate(enemyPrefabs[rID]);
            obj.transform.position = transform.position;
            obj.transform.parent = RobotParent;
            SetNextTime();
        }
	}

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
