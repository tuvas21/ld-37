﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CollisionArea : MonoBehaviour {

    void OnCollisionEnter(Collision collision)
    {
        SendMessageUpwards("Impact", collision);
    }

    void OnCollisionStay(Collision collision)
    {
        SendMessageUpwards("Impact", collision);
    }
}
