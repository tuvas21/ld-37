﻿using UnityEngine;

public class ParticleOnDead : MonoBehaviour {
    public ParticleSystem particles;
    public float y;
	
	void DeathEffect()
    {
        ParticleSystem sys = Instantiate(particles);
        Vector3 pos = transform.position;
        pos.y = y;
        sys.transform.position = pos;
        sys.Play();
        //sys.Simulate(1,false,false);
    }
}
