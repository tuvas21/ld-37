﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroManager : MonoBehaviour {

    public Rigidbody Gem;

    private float startTime;

    private void Start()
    {
        startTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            SceneManager.LoadScene("Game");
        }

        if (Time.time-startTime>10)
        {
            Gem.isKinematic = false;
        }
	}
}
