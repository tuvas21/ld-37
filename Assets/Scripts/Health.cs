﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    public float StartHealth=1;
    public float currentHealth;

    private void OnEnable()
    {
        currentHealth = StartHealth; 
    }

    void Die()
    {
        Destroy(gameObject);
        SendMessage("Dead");
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        if (currentHealth<=0)
        {
            Die();
        }
    }
}
