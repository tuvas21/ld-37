﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapTrigger : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        Trap(other);
    }

    private void OnTriggerEnter(Collider other)
    {
        Trap(other);
    }

    private void Trap(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            SendMessageUpwards("Activate");
        }
    }
}
